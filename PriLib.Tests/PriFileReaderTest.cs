﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriLib.Pers;
using PriLib.Xml;

namespace PriLib.Tests
{
    [TestClass]
    public class PriFileReaderTest
    {
        [TestMethod]
        public void TestRead()
        {
            PriFileReader.Read("E:\\Projects\\Test.pri");
        }

        [TestMethod]
        public void TestRead2()
        {
            PriFileReader.Read("E:\\Projects\\Windows.UI.Logon.Test.pri");
        }

        [TestMethod]
        public void TestLogonReplace()
        {
            PriFileReader.ModifyLogonGen2("E:\\Projects\\Windows.UI.Logon.Test.pri", "E:\\Projects\\output.pri", "E:\\Projects\\Aperture.Test.jpg");
        }

        [TestMethod]
        public void TestPriXmlReader()
        {
            PriXmlReader.Read("E:\\Projects\\schLogon.xml", PriDumpType.Schema);
        }

        [TestMethod]
        public void TestPersReader()
        {
            PersReader.Read(File.ReadAllBytes("E:\\Projects\\0.pers"));
        }

    }
}
