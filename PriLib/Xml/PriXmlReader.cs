﻿using System;
using System.Xml;

namespace PriLib.Xml
{
    public enum PriDumpType
    {
        Basic,
        Detail,
        Schema,
        Summary,
        Unknown
    }
    public class PriXmlReader
    {
        public static PriXmlSummary Read(string path, PriDumpType type)
        {
            PriXmlSummary summary = new PriXmlSummary();
            summary.DumpType = type;
            XmlDocument xml = new XmlDocument();
            xml.Load(path);
            switch (type)
            {
                case PriDumpType.Basic:
                    break;
                case PriDumpType.Detail:
                    break;
                case PriDumpType.Schema:
                    var root = xml["PriInfo"];
                    if (root == null)
                    {
                        throw new FormatException("wrong xml format");
                    }
                    summary.Root = new PriXmlObject() {CurrentNode = root};
                    foreach (XmlNode childNode in root.ChildNodes)
                    {
                        if (childNode.Name == nameof(ResourceMap))
                        {
                            
                        }
                    }
                    break;
                case PriDumpType.Summary:
                    summary = ParseSummary(xml);
                    break;
                case PriDumpType.Unknown:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            return summary;
        }
        //new XmlDocument().Load("E:\\Projects\\schLogon.xml")
        private static PriXmlSummary ParseSummary(XmlDocument xml)
        {
            PriXmlSummary summary = new PriXmlSummary();
            var root = xml["PriInfo"];
            return summary;
        }
    }
}
