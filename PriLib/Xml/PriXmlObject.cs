﻿using System.Collections.Generic;
using System.Xml;

namespace PriLib.Xml
{
    public class PriXmlObject
    {
        public XmlNode CurrentNode;
        public List<PriXmlObject> ChildNodes;
    }

    internal class VersionInfo : PriXmlObject
    {
        public string version;
        public int checksum;
        public int numScope;
        public int numItems;
    }

    internal class ResourceMap : PriXmlObject
    {
        public string name;
        public string uniqueName;
        public string version;
        public ResourceMap()
        {
            ChildNodes = new List<PriXmlObject>();
        }
    }

    internal class ResourceMapSubtree : PriXmlObject
    {
        public string name;
        public int index;

        public ResourceMapSubtree()
        {
            ChildNodes = new List<PriXmlObject>();
        }
    }

    internal class NamedResource : PriXmlObject
    {
        public string name;
        public int index;
        public byte[] Data;
    }
}
