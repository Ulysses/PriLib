﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriLib
{
    /// <summary>
    /// Version
    /// </summary>
    public struct Version
    {
        public UInt16 major;
        public UInt16 minor;
    }

    public class PriRaw
    {
        public PriHeader Header = new PriHeader();
        public PriSchema Schema = new PriSchema();
        public List<PriTocEntry> Toc = new List<PriTocEntry>();
        public class PriHeader
        {
            public string magic;
            public Version Version;
            public uint totalSize;
            public UInt16 tocNumEntries;
            /// <summary>
            /// Can be negative
            /// </summary>
            public Int16 descriptorIndex;
            public uint HeaderLength;
        }

        public class PriSchema
        {
            public string uniqueId;
            public string simpleName;
            public Version Version;
            /// <summary>
            /// checksum can be negative
            /// </summary>
            public int checksum;
            public uint numScopes;
            public uint numItems;
        }

        public class PriBlob
        {
            public long BlobTablePosition;
            public uint Offset;
            public uint Length;
            public byte[] Data;
        }

        public enum TocEntryType
        {
            Unknown,
            MrmDecnInfo,
            MrmPridescex,
            MrmHschemaex,
            MrmResMap2,
            MrmDataItem,
        }

        public class PriTocEntry
        {
            public int index;
            public string type;
            public uint flags;
            public uint sectionFlags;
            /// <summary>
            /// offset
            /// </summary>
            public uint offset;
            public uint size;
            public TocEntryType EntryType = TocEntryType.Unknown;
            /// <summary>
            /// Length of this toc, should be same as size in header.
            /// Located at attribute(16 byte) 
            /// </summary>
            public uint TocLength;

            public byte[] Data;
        }

        public class DataItemEntry : PriTocEntry
        {
            public DataItemEntry()
            {
                EntryType = TocEntryType.MrmDataItem;
            }

            public UInt16 StringCount;
            public UInt16 BlobCount;
            /// <summary>
            /// What's this?
            /// </summary>
            public uint DataLength;
            public uint DataAndContentLength;

            public long BlobTableOffset;
            public long DataOffset;
            public List<PriBlob> Blobs = new List<PriBlob>();
        }

        public class DecnInfoEntry : PriTocEntry
        {
            public DecnInfoEntry()
            {
                EntryType = TocEntryType.MrmDecnInfo;
            }
        }

        public class PridescexEntry : PriTocEntry
        {
            public PridescexEntry()
            {
                EntryType = TocEntryType.MrmPridescex;
            }
        }

        public class ResMap2Entry : PriTocEntry
        {
            public ResMap2Entry()
            {
                EntryType = TocEntryType.MrmResMap2;
            }
        }

        public class HschemaexEntry : PriTocEntry
        {

            public HschemaexEntry()
            {
                EntryType = TocEntryType.MrmHschemaex;
            }
        }

    }
}
